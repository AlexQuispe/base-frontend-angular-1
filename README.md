# Base Frontend Angular 1

Base Frontend Angular 1 es un proyecto que contiene todas las herramientas básicas para desarrollar el frontend de un nuevo proyecto con Angular 1.5.

### Tecnologías utilizadas

- ANGULAR 1.6, framework de desarrollo para el frontend.
- YEOMAN, herramienta en línea de comandos para generar aplicaciones preconfiguradas.
- ANGULAR BOOTSTRAP MATERIAL, estilo material diseñado para bootstrap.
- KARMA, para las pruebas unitarias.

### Requerimientos

- node 6.9.1
- bower `npm install -g bower`
- yeoman `npm install -g yo`
- grunt `npm install -g grunt grunt-cli`
- generator-angular `npm install -g generator-angular`

### Instalación

Para instalar el proyecto ver el archivo
 [INSTALL.md](https://gitlab.com/aquispe/base-frontend-angular-1/blob/master/INSTALL.md).

### Referencias

- Bootstrap Material Design for Angular
http://tilwinjoy.github.io/angular-bootstrap-material/
- Material Design for Bootstrap
http://fezvrasta.github.io/bootstrap-material-design/
