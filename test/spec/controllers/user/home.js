'use strict';

describe('Controller: UserHomeCtrl', function () {

  // load the controller's module
  beforeEach(module('baseApp'));

  var UserHomeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserHomeCtrl = $controller('UserHomeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
