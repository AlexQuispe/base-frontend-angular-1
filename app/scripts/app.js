'use strict';

angular
.module('baseApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'angularBootstrapMaterial'
])
.config(function ($routeProvider, $httpProvider) {
  $httpProvider.interceptors.push('TokenInterceptor');
  $routeProvider
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl',
    controllerAs: 'login',
    access: {
      requiredLogin: false
    }
  })
  .when('/administrador/inicio', {
    templateUrl: 'views/admin/home.html',
    controller: 'AdminHomeCtrl',
    controllerAs: 'admin/home',
    access: {
      requiredLogin: true
    }
  })
  .when('/kardex/inicio', {
    templateUrl: 'views/user/home.html',
    controller: 'UserHomeCtrl',
    controllerAs: 'user/home',
    access: {
      requiredLogin: true
    }
  })
  .otherwise({
    redirectTo: '/login'
  });
})
.constant('CONFIG',{
  REST_URL: 'http://10.0.0.2/cv/',
})
.run(function($rootScope, $window, $location, Authentication) {
  // cuando la pagina se actualiza, verifica si el usuario esta logueado
  Authentication.check();
  $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
    if ((nextRoute.access && nextRoute.access.requiredLogin) && !Authentication.isLogged) {
      console.log("Access denied.");
      $location.path("/login");
    } else {
      // Verifica si la informacion del usuario coincide. En caso de actualizar la pagina
      if (!Authentication.url) {
        Authentication.url = $window.sessionStorage.url;
      }
    }
  });
  $rootScope.$on('$routeChangeSuccess', function(event, nextRoute, currentRoute) {
    // Si el usuario esta logueado, lo lleva a la pagina principal
    if ((Authentication.isLogged == true) && ($location.path() == '/login')) {
      console.log("Access success.");
      $location.path(Authentication.url);
    }
  })
});
