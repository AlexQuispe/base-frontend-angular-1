'use strict';

angular.module('baseApp')
.factory('Authentication', function ($window) {
  var auth = {
    isLogged: false,
    check: function() {
      if ($window.sessionStorage.token && $window.sessionStorage.url) {
        this.isLogged = true;
        this.url = $window.sessionStorage.url;
      } else {
        this.isLogged = false;
        delete this.url;
      }
    }
  }
  return auth;
});

angular.module('baseApp')
.factory('UsuarioAuthentication', function ($window, $http, $location, Authentication, CONFIG) {
    return {
      login: function(user, pass) {
        return $http.post(CONFIG.REST_URL + 'login', {
          user: user,
          pass: pass
        });
      },
      logout: function() {
        Authentication.isLogged = false;
        delete Authentication.url;
        delete $window.sessionStorage.token;
        delete $window.sessionStorage.url;
        $location.path("/login");
      }
    }
});

// Envía el token en cada petición.
angular.module('baseApp')
.factory('TokenInterceptor', function ($q, $window) {
  return {
    request: function(config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers['Authorization'] = $window.sessionStorage.token;
        config.headers['Content-Type'] = "application/json";
      }
      return config || $q.when(config);
    },
    response: function(response) {
      return response || $q.when(response);
    }
  };
});
