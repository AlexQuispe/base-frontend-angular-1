'use strict';

angular.module('baseApp')
  .controller('LoginCtrl', function ($scope, $location, $window, Authentication, UsuarioAuthentication) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // Inicializa los efectos material
    $.material.init();

    if (Authentication.isLogged) {
      $location.path(Authentication.url);
    }

    $scope.user = '';
    $scope.pass = '';

    delete Authentication.url;

    $scope.open_page = function(location) {
      $location.path(location);
    }

    $scope.actionlogin = function() {
      UsuarioAuthentication.login($scope.user, $scope.pass)
      .then(function successCallback(response) {
        console.log("SUCCESS: " + response.data.data.url);
        Authentication.isLogged = true;
        $window.sessionStorage.token = response.data.data.token;
        $window.sessionStorage.url = response.data.data.url;
        $location.path(response.data.data.url);
      }, function errorCallback(response) {
        console.log("FAILED: " + response.data.data);
        $scope.user = "";
        $scope.pass = "";
      });
    }

  });
